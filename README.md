My current BSPWM config themed with pywal support. Mostly for personal use.

## Current Config

- Operating System
  - [Arch Linux](https://archlinux.org/)
- Window Manager
  - [BSPWM](https://github.com/baskerville/bspwm)
- Hotkey Daemon
  - [SXHKD](https://github.com/baskerville/sxhkd)
- Compositor
  - [Picom Ibhagwan](https://github.com/ibhagwan/picom)
- Bar
  - [Polybar](https://github.com/polybar/polybar)
  - [Playerctl](https://github.com/altdesktop/playerctl)
  - [Zscroll](https://github.com/noctuid/zscroll)
- Application Launcher
  - [Rofi](https://github.com/davatorium/rofi)
  - [Rofi Dmenu](https://aur.archlinux.org/packages/rofi-dmenu/)
- Notification Daemon
  - [Dunst](https://github.com/dunst-project/dunst)
- Screen Lock
  - [Betterlockscreen](https://github.com/betterlockscreen/betterlockscreen)
  - [Xidlehook](https://github.com/jD91mZM2/xidlehook)
- Display Manager
  - [SDDM](https://github.com/sddm/sddm)
  - [Clairvoyance SDDM Theme](https://github.com/eayus/sddm-theme-clairvoyance)
- Terminal Emulator
  - [Kitty](https://github.com/kovidgoyal/kitty)
- File Manager
  - [Thunar](https://github.com/xfce-mirror/thunar)
  - [Ranger](https://github.com/ranger/ranger)
- Screenshots
  - [Flameshot](https://github.com/flameshot-org/flameshot)
- Shell Prompt
  - [Starship](https://github.com/starship/starship)
- Theming
  - [Pywal](https://github.com/dylanaraps/pywal)
  - [Feh](https://github.com/derf/feh)
  - [Themix GUI Designer](https://github.com/themix-project/oomox)
  - [Neofetch](https://github.com/dylanaraps/neofetch)
- Fonts
  - [Noto Fonts](https://archlinux.org/packages/extra/any/noto-fonts/)
  - [Noto Fonts CJK](https://archlinux.org/packages/extra/any/noto-fonts-cjk/)
  - [Nerd Fonts Jetbrains Mono](https://aur.archlinux.org/packages/nerd-fonts-jetbrains-mono/)
  - [Nerd Fonts Fantasque Sans Mono](https://aur.archlinux.org/packages/nerd-fonts-fantasque-sans-mono/)

## SXHKD Shortcuts (Colemah DH)

| Function                        | Keys                                        |
| ------------------------------- | --------------------------------------------|
| Spawn Terminal                  | Super + Return                              |
| Spawn Browser                   | Super + W                                   |
| Spawn File Manager              | Super + F                                   |
| Launch Software                 | Super + Space                               |
| Lock Screen                     | Super + X                                   |
| Screenshot                      | PrtSc                                       |
| Close/Kill Window               | Super + (Shift +) Q                         |
| Quit/Restart BSPWM              | Super + Ctrl + Q/R                          |
| Toggle Tiled/Floating           | Super + S                                   |
| Toggle Sticky                   | Super + Shift + S                           |
| Toggle Fullscreen               | Super + T                                   |
| Toggle Monocle                  | Super + Shift + T                           |
| Move Node in Given Direction    | Super + Arrow Keys/Vim Keys                 |
| Send Window in Given Direction  | Super + Shift + Arrow Keys/Vim Keys         |
| Cycle Windows                   | Alt + (Shift +) Tab                         |
| Focus on Workspace              | Super + 1-0                                 |
| Send to Workspace               | Super + Shift + 1-0                         |
| Next/Previous Workspace         | Super + Alt + Arrow Keys/Vim Keys           |
| Resize Window                   | Super + Ctrl + (Shift +) Arrow Keys/Vim Keys|
| Move Floating Window            | Super + Ctrl + Alt + Arrow Keys/Vim Keys    |
| Window Selector                 | Super + Tab                                 |
| Layout Flip                     | Super + A/R                                 |
| Layout Rotation                 | Super + Shift + A/R                         |
| Play/Pause Song		  | Super + Slash				|
| Previous/Next Song		  | Super + Comma/Period			|
| Reload SXHKD Files              | Super + Escape                              |

## Screenshots

#### Desktop

![Desktop](./Desktop.png)

#### Tiling Windows

![Windows](./Windows.png)
